import { decodeBase64 } from "https://deno.land/std@0.220.1/encoding/base64.ts";

import Berries from "./upstream/src/modules/enums/BerryType.ts";
import BadgeEnums from "./upstream/src/modules/enums/Badges.ts";
import * as GameConstants from "./upstream/src/modules/GameConstants.ts";
import { GymList } from "./upstream/src/scripts/gym/GymList.ts";
import Routes from "./upstream/src/modules/routes/Routes.ts";
import { pokemonList } from "./upstream/src/modules/pokemons/PokemonList.ts";
import "./upstream/src/modules/routes/RouteData.ts";
import GameHelper from "./upstream/src/modules/GameHelper.ts";
import * as PokemonHelper from "./upstream/src/modules/pokemons/PokemonHelper.ts";

import { PartyPokemon, PartyPokemonSaveKeys } from "./upstream/src/scripts/party/PartyPokemon.ts";
import { loadSaveIntoApp } from "./upstream/src/modules/FakeApp.ts";
//import App from "./upstream/src/modules/FakeApp.ts";
//import AchievementHandler from "./upstream/src/scripts/achievements/AchievementHandler.ts";

function pad(id) {
  if (id < 10) { return `000${id}`; }
  if (id < 100) { return `00${id}`; }
  if (id < 1000) { return `0${id}`; }
  if (id < 10000) { return `${id}`; }
}

const SUPPORTED_VERSION = "0.10.23";
export function loadSave(contents: Uint8Array) {
  const save = JSON.parse(new TextDecoder().decode(decodeBase64(new TextDecoder().decode(contents))));
  if (save.save.update.version != SUPPORTED_VERSION) {
    throw `Version ${save.save.update.version} not supported. Must be ${SUPPORTED_VERSION}.`;
  }

  loadSaveIntoApp(save);
  return save;
};

class Scores {
  scores: { group: string, chart: string, value1: number|null, value2: number|null }[];

  constructor() {
    this.scores = [];
  }

  queue_score_if_positive(group: string, chart: string, score: number) {
    if (score > 0) {
      this.scores.push({ group, chart, value1: score, value2: null });
    }
  }
}

export function extractScores(save: any) {
  const game = new Scores();
  // BerryDex
  {
    save.save.statistics.berriesObtained.forEach((obtained: number, berry_id: number) => {
      game.queue_score_if_positive("Berrydex", `${Berries[berry_id]} – Obtained`, obtained);
    });
    game.queue_score_if_positive("Berrydex", "Berries Unlocked", save.save.statistics.berriesObtained.filter((c: number) => c != 0).length);
  }

  // Statistics
  {
    const stats = save.save.statistics;
    game.queue_score_if_positive("Statistics", "Quests Completed",                        stats.questsCompleted)
    game.queue_score_if_positive("Statistics", "Total Gems Gained",                       stats.totalGemsGained)
    game.queue_score_if_positive("Statistics", "Total Vitamins Obtained",                 stats.totalVitaminsObtained)
    game.queue_score_if_positive("Statistics", "Total Money",                             stats.totalMoney)
    game.queue_score_if_positive("Statistics", "Total Dungeon Tokens",                    stats.totalDungeonTokens)
    game.queue_score_if_positive("Statistics", "Total Diamonds",                          stats.totalDiamonds)
    game.queue_score_if_positive("Statistics", "Total Farm Points",                       stats.totalFarmPoints)
    game.queue_score_if_positive("Statistics", "Total Battle Points",                     stats.totalBattlePoints)
    game.queue_score_if_positive("Statistics", "Total Contest Tokens",                    stats.totalContestTokens)
    game.queue_score_if_positive("Statistics", "Total Pokemon Captured",                  stats.totalPokemonCaptured)
    game.queue_score_if_positive("Statistics", "Total Pokemon Defeated",                  stats.totalPokemonDefeated)
    game.queue_score_if_positive("Statistics", "Total Pokemon Hatched",                   stats.totalPokemonHatched)
    game.queue_score_if_positive("Statistics", "Total Shiny Pokemon Captured",            stats.totalShinyPokemonCaptured)
    game.queue_score_if_positive("Statistics", "Total Shiny Pokemon Hatched",             stats.totalShinyPokemonHatched)
    game.queue_score_if_positive("Statistics", "Total Shadow Pokemon Captured",           stats.totalShadowPokemonCaptured)
    game.queue_score_if_positive("Statistics", "Total Shadow Pokemon Defeated",           stats.totalShadowPokemonDefeated)
    game.queue_score_if_positive("Statistics", "Underground Items Found",                 stats.undergroundItemsFound)
    game.queue_score_if_positive("Statistics", "Underground Layers Mined",                stats.undergroundLayersMined)
    game.queue_score_if_positive("Statistics", "Underground Daily Deal Trades",           stats.undergroundDailyDealTrades)
    game.queue_score_if_positive("Statistics", "Total Manual Harvests",                   stats.totalManualHarvests)
    game.queue_score_if_positive("Statistics", "Total Berries Harvested",                 stats.totalBerriesHarvested)
    game.queue_score_if_positive("Statistics", "Total Berries Obtained",                  stats.totalBerriesObtained)
    game.queue_score_if_positive("Statistics", "Total Berries Mutated",                   stats.totalBerriesMutated)
    //game.queue_score_if_positive("Statistics", "Berry Daily Deal Trades",                 stats.berryDailyDealTrades)
    game.queue_score_if_positive("Statistics", "Battle Frontier Total Stages Completed",  stats.battleFrontierTotalStagesCompleted)
    game.queue_score_if_positive("Statistics", "Battle Frontier Highest Stage Completed", stats.battleFrontierHighestStageCompleted)
    game.queue_score_if_positive("Statistics", "Safari Rocks Thrown",                     stats.safariRocksThrown)
    game.queue_score_if_positive("Statistics", "Safari Bait Thrown",                      stats.safariBaitThrown)
    game.queue_score_if_positive("Statistics", "Safari Balls Thrown",                     stats.safariBallsThrown)
    game.queue_score_if_positive("Statistics", "Safari Steps Taken",                      stats.safariStepsTaken)
  }

  const regionDisplayName = (i) => {
      if (i >= GameConstants.Region.final) {
        return ['Orange League', 'Magikarp Jump', 'Orre'][i - GameConstants.Region.final];
      } else {
        return GameConstants.Region[i].charAt(0).toUpperCase() + GameConstants.Region[i].slice(1);
      }
  };

  // Badge Case
  {
    const regionBadges = (region, save) => region.map(gym => BadgeEnums[GymList[gym].badgeReward]).filter(badge => save.save.badgeCase[BadgeEnums[badge]]).filter(b => !b.startsWith('Elite') && b != 'None');
    GameConstants.RegionGyms.forEach((region, i) => {
      const region_name = regionDisplayName(i);

      const badges = regionBadges(region, save);
      game.queue_score_if_positive("Badge Case", region_name, badges.length);
    });
  }

  // Achievements: no way
  //AchievementHandler.initialize(App.game.multiplier, App.game.challenges);

  //game.queue_score_if_positive("Achievements", "Achievements Bonus", AchievementHandler.achievementBonus());


  // Dungeons
  {
    GameConstants.RegionDungeons.flat().forEach((dungeon, i) => {
      game.queue_score_if_positive("Dungeons", dungeon, save.save.statistics.dungeonsCleared[i]);
    });
  }

  // Gyms
  {
    GameConstants.RegionGyms.forEach((gyms, i) => {
      let region_name = regionDisplayName(i);
      if (region_name == "Orange League") {
        region_name = "Kanto";
      }
      if (region_name == "Magikarp Jump") {
        region_name = "Alola";
      }
      if (region_name == "Orre") {
        region_name = "Hoenn";
      }

      gyms.forEach((gym) => {
        game.queue_score_if_positive("Gyms", `${region_name} – ${GymList[gym].buttonText}`, save.save.statistics.gymsDefeated[GameConstants.getGymIndex(gym)]);
      });
    });
  }

  // Routes
  {
    Object.entries(save.save.statistics.routeKills).forEach(([region, routes]) => {
      Object.entries(routes).forEach(([route, kills]) => {
        game.queue_score_if_positive("Routes", Routes.getRoute(GameConstants.Region[region], parseInt(route, 10)).routeName, kills);
      });
    });
  }

  // Pokédex
  {
    const pokemon = save.save.party.caughtPokemon;
    const isFrom = (region) => (pokemon) => pokemonList.find((p) => p.id === pokemon.id).nativeRegion == region;
    const isShiny = (pokemon) => pokemon[PartyPokemonSaveKeys.shiny];
    const isResistant = (pokemon) => pokemon[PartyPokemonSaveKeys.pokerus] == GameConstants.Pokerus.Resistant;
    const isFormOf = (id) => (pokemon) => Math.floor(pokemon.id) == id;

    // Adapted from PokedexHelper.ts
    const hasBaseFormInSelection = (pokemon, selection) => selection.some((p) => Math.floor(p.id) == Math.floor(pokemon.id) && p.id < pokemon.id);
    const isNotAlternateInRegion = (regionPokemon) => (pokemon) => Number.isInteger(pokemon.id) || !hasBaseFormInSelection(pokemon, regionPokemon);

    game.queue_score_if_positive("Pokédex", "Pokémon Caught", pokemon.length);
    game.queue_score_if_positive("Pokédex", "Pokémon Resistant to Pokérus", pokemon.filter(isResistant).length);
    game.queue_score_if_positive("Pokédex", "Shiny Pokémon Caught", pokemon.filter(isShiny).length);

    GameHelper.enumNumbers(GameConstants.Region).forEach(region => {
      if (Number(region) >= 0) {
        const regionName = GameConstants.Region[region];

        const regionPokemon = pokemonList.filter(p => p.nativeRegion == region);
        const isNotAlternate = isNotAlternateInRegion(regionPokemon);

        const caught = pokemon.filter(isFrom(region));
        const shinyCaught = caught.filter(isShiny);
        const resistantCaught = caught.filter(isResistant);


        game.queue_score_if_positive("Pokédex", `Pokémon Caught – ${regionName}`, caught.length);
        game.queue_score_if_positive("Pokédex", `Shiny Pokémon Caught – ${regionName}`, shinyCaught.length);
        game.queue_score_if_positive("Pokédex", `Pokémon Resistant to Pokérus – ${regionName}`, resistantCaught.length);

        game.queue_score_if_positive("Pokédex", `Pokémon Caught – ${regionName} (No Alternate Forms)`, caught.filter(isNotAlternate).length);
        game.queue_score_if_positive("Pokédex", `Shiny Pokémon Caught – ${regionName} (No Alternate Forms)`, shinyCaught.filter(isNotAlternate).length);
        game.queue_score_if_positive("Pokédex", `Pokémon Resistant to Pokérus – ${regionName} (No Alternate Forms)`, resistantCaught.filter(isNotAlternate).length);
      }
    });

    game.queue_score_if_positive("Pokédex", "Pokémon Forms Caught – Pikachu", pokemon.filter(isFormOf(25)).length);
    game.queue_score_if_positive("Pokédex", "Pokémon Forms Caught – Magikarp", pokemon.filter(isFormOf(129)).length);
    game.queue_score_if_positive("Pokédex", "Pokémon Forms Caught – Unown", pokemon.filter(isFormOf(201)).length);
    game.queue_score_if_positive("Pokédex", "Pokémon Forms Caught – Vivillon", pokemon.filter(isFormOf(666)).length);
    game.queue_score_if_positive("Pokédex", "Pokémon Forms Caught – Alcremie", pokemon.filter(isFormOf(869)).length);
    game.queue_score_if_positive("Pokédex", "Pokémon Forms Caught – Furfrou", pokemon.filter(isFormOf(676)).length);
    game.queue_score_if_positive("Pokédex", "Pokémon Forms Caught – Silvally", pokemon.filter(isFormOf(773)).length);
  }

  const partyPokemon = save.save.party.caughtPokemon.map(pokemon => {
    const dataPokemon = PokemonHelper.getPokemonById(pokemon.id);
    const p = new PartyPokemon(dataPokemon.id, dataPokemon.name, dataPokemon.evolutions, dataPokemon.attack, dataPokemon.eggCycles, false, GameConstants.BattlePokemonGender.NoGender, GameConstants.ShadowStatus.None);
    p.fromJSON(pokemon);
    return p;
  });

  // Pokédex Current Attack
  partyPokemon.forEach(pokemon => {
    game.queue_score_if_positive("Pokédex Current Attack", `#${pad(pokemon.id)} – ${pokemon.name}`, pokemon.calculateAttack());
  });

  // Pokédex EVs
  partyPokemon.forEach(pokemon => {
    game.queue_score_if_positive("Pokédex EVs", `#${pad(pokemon.id)} – ${pokemon.name}`, pokemon.evs());
  });

  // Pokédex # Defeated
  Object.entries(save.save.statistics.pokemonDefeated).forEach(([pokemonId, defeats]) => {
    const exclusions = [
      "354.01",
      "334.01",
      "175.02",
      "308.01",
      "25.18",
      "25.21",
      "25.17",
      "25.2",
      "25.19",
    ];

    if (!exclusions.includes(pokemonId) && !pokemonId.startsWith("-")) {
      game.queue_score_if_positive("Pokédex # Defeated", `#${pad(pokemonId)} – ${PokemonHelper.getPokemonById(pokemonId).name}`, defeats);
    }
  });

  // Pokédex # Captured
  Object.entries(save.save.statistics.pokemonCaptured).forEach(([pokemonId, captures]) => {
    game.queue_score_if_positive("Pokédex # Captured", `#${pad(pokemonId)} – ${PokemonHelper.getPokemonById(pokemonId).name}`, captures);
  });

  // Pokédex # Captured (Shiny)
  Object.entries(save.save.statistics.shinyPokemonCaptured).forEach(([pokemonId, captures]) => {
    game.queue_score_if_positive("Pokédex # Captured (Shiny)", `#${pad(pokemonId)} – ${PokemonHelper.getPokemonById(pokemonId).name}`, captures);
  });

  return game.scores;
};
