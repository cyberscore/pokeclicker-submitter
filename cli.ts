import { extractScores, loadSave } from "./pokeclicker.ts";

export async function parse(filename: string) {
  const saveContents = loadSave(await Deno.readFile(filename));

  const scores = extractScores(saveContents);

  for (const score of scores) {
    console.log(score);
  }
}
