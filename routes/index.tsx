import { Head } from "$fresh/runtime.ts";
import { Handlers } from "$fresh/server.ts";

import { extractScores, loadSave } from "../pokeclicker.ts";

export const handler: Handlers = {
  async GET(_req, ctx) {
    return await ctx.render();
  },
  async POST(req, _ctx) {
    const form = await req.formData();
    const saveFile = form.get("file");

    if (saveFile !== null) {
      const saveContents = (await saveFile.stream().getReader().read()).value;
      const save = loadSave(saveContents);
      const scores = extractScores(save);
      return new Response(JSON.stringify({ scores }));
    } else {
      return new Response(JSON.stringify({ error: "Error parsing save file" }), { status: 400 });
    }
  },
};

export default function Home() {
  return (
    <>
      <Head>
        <title>Cyberscore - Upload Pokéclicker scores from your save file</title>
      </Head>
      <main>
        <h1>Cyberscore: Pokéclicker Save file parsing</h1>
        <p>This page has been deprecated. Pokeclicker savefile parsing has been <a href="https://cyberscore.me.uk/game/3279">moved directly into cyberscore</a>.</p>
      </main>
    </>
  );
}
