/// <reference no-default-lib="true" />
/// <reference lib="dom" />
/// <reference lib="dom.iterable" />
/// <reference lib="dom.asynciterable" />
/// <reference lib="deno.ns" />

import "$std/dotenv/load.ts";

import { start } from "$fresh/server.ts";
import manifest from "./fresh.gen.ts";
import { parse } from "./cli.ts";

if (Deno.args.length == 1) {
  await parse(Deno.args[0]);
} else {
  await start(manifest);
}
